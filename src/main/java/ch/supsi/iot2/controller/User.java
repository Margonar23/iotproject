package ch.supsi.iot2.controller;

import ch.supsi.iot2.utility.ExclusionStrategyGson;
import ch.supsi.iot2.utility.JsonHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.zeromq.ZMQ;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by vincenzo on 26/10/16.
 */
public class User extends Thing implements  Runnable{
    private String ruolo;
    private String email;
    private String nome;
    private Thing thingForInfo;
    private ArrayList<Room> rooms;
    private User() {
        this.ruolo = "";
        this.email="";
        this.nome = "";
        this.thingForInfo = null;
        setMyID("User");
    }


    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public static User createUser(){
        return new User();
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Thing getThingForInfo() {
        return thingForInfo;
    }

    public void setThingForInfo(Thing thingForInfo) {
        this.thingForInfo = thingForInfo;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    //Questo metodo non siamo riusciti a renderlo generico per problemi con la libreria gson
    //ovvero non riusciamo a ricreare la map di thing e da li recuperare Room o User
    //viene lanciata un'eccezione di cast
    public void sendRequestForGettingInformations(Thing thing) {
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket requester = context.socket(ZMQ.REQ);
        requester.connect("tcp://localhost:" + getServer().getPortForThing());
        requester.send("Information/" + thing.toJson());
        String data = new String(requester.recv(), StandardCharsets.UTF_8);
        setRequest(data);
        String[] requests = getRequest().split("/");
        Logger.getLogger("User information").info("Prova rooms: "+ requests[1]);
        Map<String,Room> maps = JsonHelper.fromJson(requests[1],new HashMap<String,Room>());
        rooms = new ArrayList<>(maps.values());
        if(consoleLog != null) {
            this.consoleLog.clear();
            for (Room room : rooms) {
                this.consoleLog.appendText(room.toString() + "\n");
            }
        }
        requester.close();
        context.term();
    }
    public User getUser(){
        return this;
    }


    public void writeProperty(Properties properties) {
        super.writeProperty(properties);
        properties.setProperty("nome",getNome());
        properties.setProperty("email",getEmail());
        properties.setProperty("myID", getMyID());
        properties.setProperty("ruolo", getRuolo());
    }

    public void readProperty(Properties properties) {
        super.readProperty(properties);
        setMyID(properties.getProperty("myId"));
        setRuolo(properties.getProperty("request"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        return ruolo != null ? ruolo.equals(user.ruolo) : user.ruolo == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (ruolo != null ? ruolo.hashCode() : 0);
        return result;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        return gson.toJson(this);
    }

    public static User fromJson(String dataEntity) {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        User entity = gson.fromJson(dataEntity, User.class);
        return entity;
    }

    @Override
    public String toString() {
        return "User{" +
                "ruolo='" + ruolo + '\'' +
                ", email='" + email + '\'' +
                ", nome='" + nome + '\'' +
                '}';
    }

    @Override
    public void run() {

        if(getRequest() == "Information"){
            sendRequestForGettingInformations(this.thingForInfo);
        }else if(getRequest() == "Registration"){
            sendRegistrationRequest();
        }

    }
}


