package ch.supsi.iot2.controller;

import ch.supsi.iot2.utility.ExclusionStrategyGson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Margonar on 19.12.2016.
 */
public class Event extends Thing implements Runnable {

    private LocalDate start;
    private String title;
    private String subtitle;
    private User supervisor;
    private Room room;
    private double price;
    private int maxSeats;
    public static ArrayList<Event> eventList = new ArrayList<>();

    public Event() {
        this.title = "";
        this.subtitle = "";
        this.supervisor = User.createUser();
        this.room = room.createRoom();
        this.price = 0;
        this.maxSeats = 0;
        setMyID("Event");
    }

    public static Event fromJson(String dataEntity) {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        Event entity = gson.fromJson(dataEntity, Event.class);
        return entity;
    }

    public static Event createEvent(){
        return new Event();
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }


    public void setSupervisor(User supervisor) {
        this.supervisor = supervisor;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }


    public String toJson() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        return gson.toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Event event = (Event) o;

        if (Double.compare(event.price, price) != 0) return false;
        if (maxSeats != event.maxSeats) return false;
        if (start != null ? !start.equals(event.start) : event.start != null) return false;
        if (title != null ? !title.equals(event.title) : event.title != null) return false;
        if (subtitle != null ? !subtitle.equals(event.subtitle) : event.subtitle != null) return false;
        if (supervisor != null ? !supervisor.equals(event.supervisor) : event.supervisor != null) return false;
        return room != null ? room.equals(event.room) : event.room == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (subtitle != null ? subtitle.hashCode() : 0);
        result = 31 * result + (supervisor != null ? supervisor.hashCode() : 0);
        result = 31 * result + (room != null ? room.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + maxSeats;
        return result;
    }

//@TODO capire se serve avere read&write property method

    @Override
    public void run() {
        sendRegistrationRequest();
    }
}
