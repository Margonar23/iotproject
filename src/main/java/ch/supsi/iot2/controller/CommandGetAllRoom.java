package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.builder.UserBuilder;
import javafx.scene.control.TextArea;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandGetAllRoom implements CommandController {
    private Thread sender;

    @Override
    public void execute() {
        sender.start();
    }

    @Override
    public void undo() {
        while (!sender.isInterrupted())
            sender.interrupt();
    }
    private CommandGetAllRoom(Runnable entity){
        this.sender = new Thread(entity);
    }

    public static CommandGetAllRoom create(TextArea consoleLog){
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(5000);
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(consoleLog)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        UserBuilder userBuilder = new UserBuilder();
        User user = userBuilder.build();
            userBuilder.setRequest("Information")
                        .setThingForInfo(room)
                        .setServer(server)
                    .setConsoleLog(consoleLog);

        return new CommandGetAllRoom(user);
    }

    public Thread getSender() {
        return sender;
    }

}
