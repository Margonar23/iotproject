package ch.supsi.iot2.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ch.supsi.iot2.utility.ExclusionStrategyGson;
import javafx.scene.control.TextArea;
import org.zeromq.ZMQ;

import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by vincenzo on 19/10/16.
 */
public class Thing{

    public ZeroMqServer getServer() {
        return server;
    }

    public void setServer(ZeroMqServer server) {
        this.server = server;
    }

    private ZeroMqServer server = null;
    protected TextArea consoleLog = null;
    private String myID;
    private String key;
    private String request;

    public void setKey(String key) {
        this.key = key;
    }

    public String getMyID() {
        return myID;
    }

    public void setMyID(String myID) {
        this.myID = myID;
    }

    public String getKey() {
        return key;
    }


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        return gson.toJson(this);
    }

    public TextArea getConsoleLog() {
        return consoleLog;
    }

    public void setConsoleLog(TextArea consoleLog) {
        this.consoleLog = consoleLog;
    }

    public static Thing fromJson(String dataEntity) {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        Thing entity = gson.fromJson(dataEntity, Thing.class);
        return entity;
    }

    public void sendRegistrationRequest() {
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket requester = context.socket(ZMQ.REQ);
        requester.connect("tcp://localhost:" + server.getPortForThing());
        String msg = toJson();
        requester.send(getRequest() + "/" + msg);
        String key = new String(requester.recv(), StandardCharsets.UTF_8);
        this.setKey(key);
        Logger.getLogger("Thing").info("Key received" + this.getKey());
        if (getConsoleLog() != null)
            getConsoleLog().setText(this.getKey());
        requester.close();
        context.term();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Thing thing = (Thing) o;

        if (key != null ? !key.equals(thing.key) : thing.key != null) return false;
        return myID != null ? myID.equals(thing.myID) : thing.myID == null;

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (myID != null ? myID.hashCode() : 0);
        return result;
    }


    public void writeProperty(Properties properties) {
    }


    public void readProperty(Properties properties) {
    }

}
