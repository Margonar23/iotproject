package ch.supsi.iot2.controller;


import ch.supsi.iot2.model.Driver;
import javafx.scene.control.TextArea;
import org.zeromq.ZMQ;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Created by Grem on 14.10.16.
 * <p>
 */
public class ZeroMqServer{
    private Logger infoLogger = Logger.getLogger("Server Logger");
    private int portForThing;
    private int portForModulePub;
    private int portForModuleReq;
    private int portForPubDriver;
    private int portForReqDriver;
    private TextArea consoleLog = null;
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Socket client_server = null;
    private ZMQ.Socket server_client = null;
    private ZMQ.Socket server_module = null;
    //Questi due socket vengono passati ai moduli, vengono creati qui perche vi deve essere un bind unico per tutti i moduli.
    private ZMQ.Socket module_driver = null;
    private ZMQ.Socket driver_Module = null;
    private String dataAtThings = null;
    private String key = null;
    private AtomicInteger lineCount = new AtomicInteger(0);
    private volatile ArrayList<Driver> drivers;
    private volatile static ZeroMqServer instance;

    public void avviaServer(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                while (!Thread.currentThread().isInterrupted()) {
                    count++;
                    Logger.getLogger("Server Log").info("Server is running: " + count);
                    writeLog("Server is running: " + count);
                    listenEntityRequests();
                    pubRequest(manageEntityRequests());
                    listenDataForClient();
                    while (key == null) {
                        System.out.flush();
                    }
                    sendDataToClient();

                }
                closeConnection();
                context.term();
            }
        }).start();
    }

    public  static synchronized ZeroMqServer getInstance() {
       if (instance == null)
           instance = new ZeroMqServer();
        return instance;
    }



    public int getPortForThing() {
        return portForThing;
    }



    public int getPortForModulePub() {
        return portForModulePub;
    }


    public int getPortForModuleReq() {
        return portForModuleReq;
    }


    public int getPortForPubDriver() {
        return portForPubDriver;
    }


    public int getPortForReqDriver() {
        return portForReqDriver;
    }

    public TextArea getConsoleLog() {
        return consoleLog;
    }


    public void setConsoleLog(TextArea consoleLog) {
        this.consoleLog = consoleLog;
    }

    private ZeroMqServer() {
        this.drivers = new ArrayList<>();
    }


    private  boolean controlValidationPort(final int connectionPort) {
        if (connectionPort < 5000 && connectionPort >= 7000)
            return false;
        else return true;
    }

    public void setConnectionPort(final int port){
        if(controlValidationPort(port)){
            this.portForThing = port;
            this.portForModulePub = port+1;
           this.portForModuleReq = port+2;
            this.portForPubDriver = port+3;
            this.portForReqDriver = port+4;
            writeLog("Server: configuration ports complete");
        }else{
            writeLog("Server: port not valid");
        }
    }


    public void serverInit(ArrayList<Module> modules) {
        this.client_server = context.socket(ZMQ.REP);
        Logger.getLogger("ff").info(portForThing+""); Logger.getLogger("ff").info(portForModulePub+"");
        this.client_server.bind("tcp://*:" + portForThing);
        this.server_client = context.socket(ZMQ.REP);
        this.server_client.bind("tcp://*:" + portForModuleReq);
        this.server_module = context.socket(ZMQ.PUB);
        this.server_module.bind("tcp://*:" + portForModulePub);
        this.module_driver = context.socket(ZMQ.PUB);
        this.module_driver.bind("tcp://*:" + portForPubDriver);
        this.driver_Module = context.socket(ZMQ.REP);
        this.driver_Module.bind("tcp://*:" + portForReqDriver);
        if(modules != null) {
            for (Module module : modules
                    ) {
                module.setServerZMQ(this);
            }
        }
    }

    public void closeConnection() {
        client_server.close();
        server_client.close();
        server_module.close();
        module_driver.close();
        driver_Module.close();
    }

    public void listenEntityRequests() {
        byte[] request = this.client_server.recv();
        String data = new String(request, StandardCharsets.UTF_8);
        Logger.getLogger("Server Log").info("Server: request received!!");
        writeLog("Server: request received!!");
        this.dataAtThings = data;

    }

    public String manageEntityRequests() {
        String[] requests = dataAtThings.split("/");
        String request = requests[0];
        this.dataAtThings = requests[1];
        return request.trim();
    }


    public void pubRequest(String request) {
        while (!Module.subOK) {
            server_module.sendMore(request);
            server_module.send(this.dataAtThings);
        }
        Logger.getLogger("Pub server").info("Server : Pub fatto");
        writeLog("Server : Pub fatto");
        Module.subOK = false;
    }

    public void listenDataForClient() {
        String data = new String(server_client.recv(), StandardCharsets.UTF_8);
        this.key = data;
        server_client.send("200");
        Logger.getLogger("Server Log").info("Server: Received response from Module " + this.key);
        writeLog("Server: Received response from Module " + this.key);
    }

    public void sendDataToClient() {
        this.client_server.send(this.key);
    }

    public String getDataAtThings() {
        return dataAtThings;
    }

    public String getKey() {
        return key;
    }


    public ZMQ.Socket getModule_driver() {
        return module_driver;
    }


    public ZMQ.Socket getDriver_Module() {
        return driver_Module;
    }


    public void setDataAtThings(String dataAtThings) {
        this.dataAtThings = dataAtThings;
    }

    public boolean controlIfConsoleExists(){
        return consoleLog != null;
    }

    public synchronized void writeLog(String text) {
        if(controlIfConsoleExists()) {
            getConsoleLog().appendText(textFormatted(text));
            lineCount.incrementAndGet();
        }else
            Logger.getLogger("Console Server").warning("Non vi è impostata la console su cui scrivere");
    }

    public String textFormatted(String text) {
        String finalString = lineCount.get() + "> " + "|TIME: " + new SimpleDateFormat("HH.mm.ss").format(new Date()) + "| MESSAGE: " + text + "\n";
        return finalString;
    }

    public void addDriver(Driver driver){
        this.drivers.add(driver);
    }
    public ArrayList<Driver> getDrivers() {
        return this.drivers;
    }

    public void saveEntity(){
        for (Driver d : drivers) {
            d.save();
        }
        writeLog("Server: saved Complete");
    }

    public void loadEntity(){
        for (Driver d:drivers
             ) {
            d.load();
        }
        writeLog("Server: loaded Complete");
    }
}
