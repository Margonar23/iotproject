package ch.supsi.iot2.controller;

/**
 * Created by Margonar on 17.10.2016.
 */

import ch.supsi.iot2.utility.Utility;

import java.util.logging.Logger;

/**
 * Pubsub envelope publisher
 */

public class RegModule extends Module{

    public RegModule(){
        setIdModule("Registration");
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            moduleInit();
            Logger.getLogger("Info Module").info("Reg Module start . . .");
            subscribeRequest();
            pubToDriver(this.getTypeEntity(), this.getDataEntity());
            sendDataToServer("Registration Success/" + new Utility().keyGenerator());
        }
        getContext().term();
    }


}