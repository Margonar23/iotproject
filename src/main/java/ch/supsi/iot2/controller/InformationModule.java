package ch.supsi.iot2.controller;

/**
 * Created by Margonar on 17.10.2016.
 */

import java.util.logging.Logger;


public class InformationModule extends Module{

    public InformationModule() {
        setIdModule("Information");
    }


    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            moduleInit();
            Logger.getLogger("Info Module").info("Information Module start . . .");
            subscribeRequest();
            pubToDriver(this.getTypeEntity(),this.getDataEntity());
            listenInformationFromDriver();
            sendDataToServer(this.getDataEntity());
        }
        getContext().term();
    }

    public void listenInformationFromDriver() {
        this.setDataEntity(this.getServerZMQ().getDriver_Module().recvStr());
        getServerZMQ().writeLog("Information Module received information from driver"+ this.getDataEntity());
        this.getServerZMQ().getDriver_Module().send("200");
    }




}