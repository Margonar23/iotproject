package ch.supsi.iot2.controller;

/**
 * Created by Grem on 11.01.17.
 */
public interface CommandController {
    void execute();
    void undo();
}
