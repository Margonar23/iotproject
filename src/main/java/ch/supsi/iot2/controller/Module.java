package ch.supsi.iot2.controller;

import ch.supsi.iot2.model.Driver;
import org.zeromq.ZMQ;

import java.util.logging.Logger;

/**
 * Created by Grem on 24.11.16.
 */
public  abstract class Module implements Runnable {
    public static volatile boolean subOK = false;

    public  String getIdModule() {
        return this.idModule;
    }

    public  void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public  String idModule = null;
    private ZeroMqServer serverZMQ = null;
    private ZMQ.Context context;
    private ZMQ.Socket subscriber;
    private ZMQ.Socket sender;
    private String dataEntity = null;
    private int codeKeySended = 0;
    private String typeEntity = null;

    public void moduleInit(){
        context = ZMQ.context(1);
        subscriber = context.socket(ZMQ.SUB);
        sender = context.socket(ZMQ.REQ);
    }

    public void subscribeRequest() {
        subscriber.connect("tcp://localhost:" + getServerZMQ().getPortForModulePub());
        subscriber.subscribe(this.idModule.getBytes());
        subscriber.recvStr();
        dataEntity = subscriber.recvStr();
        setTypeEntity();
        subOK = true;
        Logger.getLogger("RegModule Log").info("RegModule subscribe request: "+dataEntity);
        getServerZMQ().writeLog(this.idModule+ " subscribe request: "+dataEntity);
    }

    public void pubToDriver(String typeEntity, String dataEntity) {
        while (!Driver.subDriverOk) {
            this.getServerZMQ().getModule_driver().sendMore(typeEntity);
            this.getServerZMQ().getModule_driver().send(dataEntity);
        }
        Driver.subDriverOk = false;
        Logger.getLogger("RegModule Log").info("RegModule pub request");
        getServerZMQ().writeLog(this.idModule+" pub request to driver");
    }

    public void sendDataToServer(String data){
        this.getSender().connect("tcp://localhost:" + getServerZMQ().getPortForModuleReq());
        this.getSender().send(data);
        this.codeKeySended = Integer.parseInt(this.getSender().recvStr());
        getServerZMQ().writeLog(this.idModule+" Module sended inormation to server, STATUS: "+this.codeKeySended);
    }
    public ZMQ.Context getContext() {
        return context;
    }

    public ZMQ.Socket getSender() {
        return sender;
    }



    public String getDataEntity() {
        return dataEntity;
    }

    public void setDataEntity(String dataEntity) {
        this.dataEntity = dataEntity;
    }

    public int getCodeKeySended() {
        return codeKeySended;
    }


    public String getTypeEntity() {
        return typeEntity;
    }

    private void setTypeEntity() {
        Thing entity = Thing.fromJson(dataEntity);
        this.typeEntity = entity.getMyID();
        Logger.getLogger("RegModule Log").info("RegModule "+this.typeEntity);
    }

    public  ZeroMqServer getServerZMQ() {
        return this.serverZMQ;
    }

    public  void setServerZMQ(ZeroMqServer serverZMQ) {
        this.serverZMQ = serverZMQ;
    }

}
