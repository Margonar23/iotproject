package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.EventBuilder;
import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.builder.UserBuilder;
import javafx.scene.control.TextArea;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandSendRegistration implements CommandController {
    private Thread sender;

    @Override
    public void execute() {
        sender.start();
    }

    @Override
    public void undo() {
        while (!sender.isInterrupted())
        sender.interrupt();
    }

    private CommandSendRegistration(Runnable entity){
        sender = new Thread(entity);
    }

    public static CommandSendRegistration create(String nomeUser, String email, String ruolo, TextArea consoleLog){
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(5000);
        UserBuilder builder = new UserBuilder();
        User user = builder.build();
        builder.setNome(nomeUser)
                .setEmail(email)
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(consoleLog)
                .setRuolo(ruolo)
                .setServer(server);

        return  new CommandSendRegistration(user);

    }
    public static CommandSendRegistration create(String title, String subTitle, LocalDate date,double price,Room room,User user,int maxSeats, TextArea consoleLog){
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(5000);
        EventBuilder builder = new EventBuilder();
        Event event = builder.build();
        builder.setTitle(title)
                .setSubtitle(subTitle)
                .setStart(date)
                .setSupervisor(user)
                .setRoom(room)
                .setPrice(price)
                .setMaxSeats(maxSeats)
                .setServer(server)
                .setConsoleLog(consoleLog)
                .setRequest("Registration");

        return  new CommandSendRegistration(event);

    }
    public static CommandSendRegistration create(String nChairs, String nFloor, String nRoom, boolean hasBeamer, boolean hasComputer, TextArea consoleLog){
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(5000);
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        try {
            builder.setChair( NumberFormat.getInstance().parse(nChairs).intValue())
                    .setFloor(NumberFormat.getInstance().parse(nFloor).intValue())
                    .setNumber(NumberFormat.getInstance().parse(nRoom).intValue())
                    .setHasBeamer(hasBeamer)
                    .setHasComputer(hasComputer)
                    .setConsoleLog(consoleLog)
                    .setServer(server)
                    .setRequest("Registration")
                    .setMyId();
        } catch (ParseException e) {
            e.printStackTrace();
            consoleLog.setText("ERRORE INSERIMENTO DATI");
        }
        return new CommandSendRegistration(room);
    }

    public Thread getSender() {
        return sender;
    }
}
