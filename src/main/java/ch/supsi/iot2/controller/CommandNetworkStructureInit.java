package ch.supsi.iot2.controller;

import ch.supsi.iot2.model.*;
import javafx.scene.control.TextArea;

import java.util.ArrayList;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandNetworkStructureInit implements CommandController {
    private Thread regModule;
    private Thread infoModule;
    private Thread roomDriver;
    private Thread userDriver;
    private Thread eventDriver;
    private ZeroMqServer server = ZeroMqServer.getInstance();

    private CommandNetworkStructureInit(Runnable regModule,Runnable infoModule,Runnable roomDriver,Runnable userDriver,Runnable eventDriver){
        this.regModule = new Thread(regModule);
        this.infoModule = new Thread(infoModule);
        this.roomDriver = new Thread(roomDriver);
        this.userDriver = new Thread(userDriver);
        this.eventDriver = new Thread(eventDriver);
    }

    @Override
    public void execute() {
        this.server.avviaServer();
        this.userDriver.start();
        this.roomDriver.start();
        this.infoModule.start();
        this.regModule.start();
        this.eventDriver.start();
    }
    public static CommandNetworkStructureInit create(TextArea console){
        ZeroMqServer.getInstance().setConsoleLog(console);
        ZeroMqServer.getInstance().setConnectionPort(5000);
        Module module1 = new RegModule();
        Module module2 = new InformationModule();
        ArrayList<Module> modules = new ArrayList<>();
        modules.add(module1);
        modules.add(module2);
        ZeroMqServer.getInstance().serverInit(modules);
        return new CommandNetworkStructureInit(module1,module2,createRoomDriver(),createUserDriver(),createEventDriver());

    }
    private static UserDriver createUserDriver(){
        UserDriver driver = new UserDriver();
        driver.setServer(ZeroMqServer.getInstance());
        return driver;
    }
    private static RoomDriver createRoomDriver(){
        RoomDriver driver = new RoomDriver();
        driver.setServer(ZeroMqServer.getInstance());
        return driver;
    }
    private static EventDriver createEventDriver(){
        EventDriver driver = new EventDriver();
        driver.setServer(ZeroMqServer.getInstance());
        return driver;
    }

    @Override
    public void undo() {
        while (!this.regModule.isInterrupted())
            this.regModule.interrupt();
        while (!this.infoModule.isInterrupted())
            this.infoModule.interrupt();
        while (!this.roomDriver.isInterrupted())
            this.roomDriver.interrupt();
        while (!this.userDriver.isInterrupted())
            this.userDriver.interrupt();
        while (!this.eventDriver.isInterrupted())
            this.eventDriver.interrupt();
        System.exit(200);
    }

    public Thread getRegModule() {
        return regModule;
    }

    public Thread getInfoModule() {
        return infoModule;
    }

    public Thread getRoomDriver() {
        return roomDriver;
    }

    public Thread getUserDriver() {
        return userDriver;
    }

    public Thread getEventDriver() {
        return eventDriver;
    }
}
