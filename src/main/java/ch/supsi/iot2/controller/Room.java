package ch.supsi.iot2.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ch.supsi.iot2.utility.ExclusionStrategyGson;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by vincenzo on 26/10/16.
 */
public class Room extends Thing implements  Runnable{
    private int chair;
    private int floor;
    private int number;
    private boolean hasBeamer;
    private boolean hasComputer;

    public ArrayList<LocalDate> availabilty = new ArrayList<>();


    private Room() {
        this.chair = 0;
        this.floor=0;
        this.number=0;
        this.hasBeamer=false;
        this.hasComputer=false;
        setMyID("Room");
    }
    public static Room createRoom(){
        return new Room();
    }

    public static Room fromJson(String dataEntity) {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        Room entity = gson.fromJson(dataEntity, Room.class);
        return entity;
    }

    public int getChair() {
        return chair;
    }

    public void setChair(int chair) {
        this.chair = chair;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isHasBeamer() {
        return hasBeamer;
    }

    public void setHasBeamer(boolean hasBeamer) {
        this.hasBeamer = hasBeamer;
    }

    public boolean isHasComputer() {
        return hasComputer;
    }

    public void setHasComputer(boolean hasComputer) {
        this.hasComputer = hasComputer;
    }


    public Room getRoom(){
        return this;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        return gson.toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Room room = (Room) o;

        if (chair != room.chair) return false;
        if (floor != room.floor) return false;
        if (number != room.number) return false;
        if (hasBeamer != room.hasBeamer) return false;
        return hasComputer == room.hasComputer;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + chair;
        result = 31 * result + floor;
        result = 31 * result + number;
        result = 31 * result + (hasBeamer ? 1 : 0);
        result = 31 * result + (hasComputer ? 1 : 0);
        return result;
    }

    public void writeProperty(Properties properties) {

        properties.setProperty("chair", String.valueOf(chair));
        properties.setProperty("floor", String.valueOf(floor));
        properties.setProperty("number", String.valueOf(number));
        properties.setProperty("hasBeamer", String.valueOf(hasBeamer));
        properties.setProperty("hasComputer", String.valueOf(hasComputer));
        properties.setProperty("myId", getMyID());

    }

    public void readProperty(Properties properties) {
        setChair(Integer.parseInt(properties.getProperty("chair")));
        setFloor(Integer.parseInt(properties.getProperty("floor")));
        setNumber(Integer.parseInt(properties.getProperty("number")));
        setHasBeamer(Boolean.parseBoolean(properties.getProperty("hasBeamer")));
        setHasComputer(Boolean.parseBoolean(properties.getProperty("hasComputer")));
    }

    @Override
    public String toString() {
        return "Room{" +
                "number =" + number +
                ", floor =" + floor +
                ", chair =" + chair +
                ", hasBeamer ="+hasBeamer+
                ", hasComputer ="+hasComputer+
                '}';
    }

    @Override
    public void run() {
        sendRegistrationRequest();
    }
}
