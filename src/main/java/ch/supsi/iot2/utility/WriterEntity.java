package ch.supsi.iot2.utility;

import ch.supsi.iot2.controller.Thing;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.util.*;

/**
 * Created by vincenzo on 27/10/16.
 */
public class WriterEntity {
    private static File file;
    private static Writer output = null;
    private static FileReader input = null;


    public static void write(String nameFile, Map<String, Thing> entity){
        file = new File(nameFile);

        try {
            output = new FileWriter(file);
            JsonHelper.toJson(entity,output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    public static Map<String, Thing> load(String fileName, Map<String,Thing> maps, Class<?> className) {
        file = new File(fileName);
        try {
            input = new FileReader(file);
            JsonReader reader = new JsonReader(input);
            maps = JsonHelper.fromJson(reader, maps);
            System.out.println(maps.size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }
}
