package ch.supsi.iot2.utility;

import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.Thing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.Writer;
import java.util.Map;

/**
 * Created by vincenzo on 30/12/16.
 */
public class JsonHelper {
    private static Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
    public static void toJson(Map<String,Thing> map, Writer writer){
        gson.toJson(map,writer);
    }
    public static Map<String,Thing>  fromJson(JsonReader reader, Map<String, Thing> map){
        return gson.fromJson(reader,map.getClass());
    }
    public static String toJson(Map<String,Thing> map){
        return gson.toJson(map);
    }

    public static Map<String,Room>  fromJson(String reader, Map<String, Room> map){
        return gson.fromJson(reader,new TypeToken<Map<String,Room>>() {}.getType());
    }

}
