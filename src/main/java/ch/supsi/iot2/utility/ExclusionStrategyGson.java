package ch.supsi.iot2.utility;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import ch.supsi.iot2.controller.Thing;

/**
 * Created by vincenzo on 27/10/16.
 */
public class ExclusionStrategyGson implements ExclusionStrategy {


    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        if (f.getDeclaringClass() == Thing.class && f.getName().equals("consoleLog")) {
            return true;
        } else if (f.getDeclaringClass() == Thing.class && f.getName().equals("server")) {
            return true;
        } else {
            return false;
        }
    }

}
