package ch.supsi.iot2.utility;

import java.util.UUID;

/**
 * Created by Margonar on 18.10.2016.
 */
public class Utility {
    public static String keyGenerator(){
       return UUID.randomUUID().toString();
    }
}
