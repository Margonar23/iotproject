package ch.supsi.iot2.builder;

import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.ZeroMqServer;
import javafx.scene.control.TextArea;


/**
 * Created by Margonar on 05.12.2016.
 */
public class RoomBuilder {


    private static Room room;

    public Room build() {
        room = Room.createRoom();
        return room;
    }

    public RoomBuilder setMyId() {
        room.setMyID("Room");
        return this;
    }

    public RoomBuilder setChair(int chairs) {
        room.setChair(chairs);
        return this;
    }

    public RoomBuilder setFloor(int floor) {
        room.setFloor(floor);
        return this;
    }

    public RoomBuilder setNumber(int number) {
        room.setNumber(number);
        return this;
    }

    public RoomBuilder setHasBeamer(boolean hasBeamer) {
        room.setHasBeamer(hasBeamer);
        return this;
    }

    public RoomBuilder setHasComputer(boolean hasComputer) {
        room.setHasComputer(hasComputer);
        return this;
    }

    public RoomBuilder setRequest(String request) {
        room.setRequest(request);
        return this;
    }

    public RoomBuilder setConsoleLog(TextArea consoleLog) {
        room.setConsoleLog(consoleLog);
        return this;
    }
    public RoomBuilder setServer(ZeroMqServer server) {
        room.setServer(server);
        return this;
    }


}
