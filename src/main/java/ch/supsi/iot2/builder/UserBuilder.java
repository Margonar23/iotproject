package ch.supsi.iot2.builder;

import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.controller.User;
import ch.supsi.iot2.controller.ZeroMqServer;
import javafx.scene.control.TextArea;

/**
 * Created by Margonar on 07.12.2016.
 */
public class UserBuilder {

    private static User user;

    public static User build(){
        user = User.createUser();
        return user;
    }

    public UserBuilder setMyID(){
        user.setMyID("User");
        return this;
    }

    public UserBuilder setRuolo(String ruolo){
        user.setRuolo(ruolo);
        return this;
    }

    public UserBuilder setRequest(String request){
        user.setRequest(request);
        return this;
    }
    public UserBuilder setConsoleLog(TextArea consoleLog) {
        user.setConsoleLog(consoleLog);
        return this;
    }

    public UserBuilder setEmail(String email){
        user.setEmail(email);
        return this;
    }

    public UserBuilder setNome(String nome){
        user.setNome(nome);
        return this;
    }

    public UserBuilder setThingForInfo(Thing t){
        user.setThingForInfo(t);
        return this;
    }
    public UserBuilder setServer(ZeroMqServer server){
        user.setServer(server);
        return this;
    }
}

