package ch.supsi.iot2.builder;

import ch.supsi.iot2.controller.Event;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.User;
import ch.supsi.iot2.controller.ZeroMqServer;
import javafx.scene.control.TextArea;

import java.time.LocalDate;

/**
 * Created by Margonar on 19.12.2016.
 */

public class EventBuilder {
    private static Event event;

    public Event build() {
        event = Event.createEvent();
        return event;
    }

    public EventBuilder setStart(LocalDate start){
        event.setStart(start);
        return this;
    }

    public EventBuilder setTitle(String title){
        event.setTitle(title);
        return this;
    }

    public EventBuilder setSubtitle(String subtitle){
        event.setSubtitle(subtitle);
        return this;
    }

    public EventBuilder setSupervisor(User supervisor){
        event.setSupervisor(supervisor);
        return this;
    }

    public EventBuilder setRoom(Room room){
        event.setRoom(room);
        if(room!=null)
        room.availabilty.add(event.getStart());
        return this;
    }

    public EventBuilder setPrice(double price){
        event.setPrice(price);
        return this;
    }

    public EventBuilder setMaxSeats(int maxSeats){
        event.setMaxSeats(maxSeats);
        return this;
    }

    public EventBuilder setRequest(String request) {
        event.setRequest(request);
        return this;
    }

    public EventBuilder setConsoleLog(TextArea consoleLog) {
        event.setConsoleLog(consoleLog);
        return this;
    }
    public EventBuilder setServer(ZeroMqServer server) {
        event.setServer(server);
        return this;
    }
}
