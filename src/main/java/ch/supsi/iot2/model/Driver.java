package ch.supsi.iot2.model;

import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.controller.ZeroMqServer;
import ch.supsi.iot2.utility.WriterEntity;
import org.zeromq.ZMQ;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Grem on 30.11.16.
 */
public abstract class Driver {
    public static volatile boolean subDriverOk = false;
    private ZeroMqServer server = null;

    private ZMQ.Context context = null;
    private String idModule;
    private Map<String, Thing> maps = new HashMap<>();
    private ZMQ.Socket subscriber = null;
    private ZMQ.Socket sender = null;

    public void setMaps(Map<String, Thing> maps) {
        this.maps = maps;
    }

    public void setIdModule(String idModule) {

        this.idModule = idModule;
    }

    public String getIdModule() {
        return idModule;
    }

    private int codeStatusSend = 0;

    public void driverInit(){
        context = ZMQ.context(1);
        subscriber = context.socket(ZMQ.SUB);
        sender = context.socket(ZMQ.REQ);
    }

    protected String subscribe() {
        driverInit();
        subscriber.connect("tcp://localhost:" + server.getPortForPubDriver());
        subscriber.subscribe(idModule.getBytes());
        subscriber.recvStr();
        String receive = subscriber.recvStr();
        return receive;

    }

    public int getCodeStatusSend() {
        return codeStatusSend;
    }

    public void sendInformations(String information) {
        sender.connect("tcp://localhost:" + server.getPortForReqDriver());
        sender.send(information);
        codeStatusSend = Integer.parseInt(sender.recvStr());

    }

    public ZeroMqServer getServer() {
        return server;
    }

    public void setServer(ZeroMqServer server) {
        this.server = server;
        server.addDriver(this);


    }

    public Map<String, Thing> getMaps() {
        return maps;
    }

    public abstract void save();
    public abstract void load();


}
