package ch.supsi.iot2.model;

import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.utility.JsonHelper;
import ch.supsi.iot2.utility.WriterEntity;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created by Grem on 03.11.16.
 */
public class RoomDriver extends Driver implements Runnable {
    String received;

    public String getReceived() {
        return received;
    }

    public RoomDriver() {
            setIdModule("Room");
    }

    public Room subscribeRequest() {
            this.received = subscribe();
            Room room = Room.fromJson(received);
            subDriverOk = true;
        return room;
    }

    public void manageRequest(Room room){
        switch (room.getRequest()) {
            case ("Registration"):
                Logger.getLogger("Room Driver Log").info("RoomDriver subscribe registration");
                getServer().writeLog("Room Driver subscribe a registration");
                registration(room);
                break;
            case ("Information"):
                getServer().writeLog("Room Driver subscribe a information");
                manageInformation(room);
                break;
        }
    }

    private void manageInformation(Room room) {
        if (room.getNumber() == 0) {
            sendInformations("Tutte le room/" + getAllInformation(room));
        } else
            sendInformations("Room Specifica"+getInformation(room));

    }

    public void registration(Room room) {
        getMaps().put(room.getNumber()+"", room);
    }


    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            manageRequest(subscribeRequest());
        }
    }

    public String getInformation(Room room) {
        HashMap<String,Thing> mapToSender = new HashMap<String, Thing>();
        mapToSender.put(room.getNumber()+"",getMaps().get(room.getNumber()+""));
        return JsonHelper.toJson(mapToSender);

    }
    public String getAllInformation(Room room) {
        return JsonHelper.toJson(getMaps());
    }


    public void save(){
        WriterEntity.write("Room.txt",getMaps());
    }

    @Override
    public void load() {
        setMaps(WriterEntity.load("Room.txt",getMaps(),Room.class));
    }


}
