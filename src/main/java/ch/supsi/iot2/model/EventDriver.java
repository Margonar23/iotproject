package ch.supsi.iot2.model;

import ch.supsi.iot2.controller.Event;
import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.utility.JsonHelper;
import ch.supsi.iot2.utility.WriterEntity;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created by Grem on 15.01.17.
 */
public class EventDriver extends Driver implements Runnable {

    String received;

    public EventDriver() {
        setIdModule("Event");
    }

    public Event subscribeRequest() {
        this.received = subscribe();
        Event event = Event.fromJson(received);
        subDriverOk = true;
        return event;
    }

    public void manageRequest(Event event){
        switch (event.getRequest()) {
            case ("Registration"):
                Logger.getLogger("Room Driver Log").info("EventDriver subscribe registration");
                getServer().writeLog("Event Driver subscribe a registration");
                registration(event);
                break;
            case ("Information"):
                getServer().writeLog("Event Driver subscribe a information");
                manageInformation(event);
                break;
        }
    }

    private void manageInformation(Event event) {
        if (event.getTitle().equals("")) {
            sendInformations("Tutte le room/" + getAllInformation(event));
        } else
            sendInformations("Room Specifica"+getInformation(event));

    }

    public void registration(Event event) {
        getMaps().put(event.getTitle()+"", event);
    }


    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            manageRequest(subscribeRequest());
        }
    }

    public String getInformation(Event event) {
        HashMap<String,Thing> mapToSender = new HashMap<String, Thing>();
        mapToSender.put(event.getTitle()+"",getMaps().get(event.getTitle()+""));
        return JsonHelper.toJson(mapToSender);

    }
    public String getAllInformation(Event event) {
        return JsonHelper.toJson(getMaps());
    }

    @Override
    public void save(){
        WriterEntity.write("Event.txt",getMaps());
    }

    @Override
    public void load() {
        setMaps(WriterEntity.load("Event.txt",getMaps(),Event.class));
    }


}
