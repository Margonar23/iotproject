package ch.supsi.iot2.model;

import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.controller.User;
import ch.supsi.iot2.utility.WriterEntity;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Margonar on 03.11.16.
 */
public class UserDriver extends Driver implements Runnable {

 public UserDriver() {
        setIdModule("User");
    }

    public User subscribeRequest() {
        String receive = subscribe();
            User user = User.fromJson(receive);
            subDriverOk = true;
        return user;
    }

    public void manageRequest(User user){
        switch (user.getRequest()) {
            case ("Registration"):
                Logger.getLogger("User Driver Log").info("User Driver subscribe registration");
                registration(user);
                break;
            case ("Information"):
                manageInformation(user);
                break;

        }
    }
    private void manageInformation(User user){
        //@TODO da cambiare
        if(user.getEmail() == "") {
            sendInformations("Tutte le user/" + user.toJson());
        }else
            sendInformations("User Specifica");

    }
    public void registration(User user) {
        getMaps().put(user.getEmail(),user);
    }


    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
        manageRequest(subscribeRequest());
        }

    }

    public String getInformation(User user) {
        return user.toJson();
    }

    public void save(){

        WriterEntity.write("User.txt",getMaps());
    }

    @Override
    public void load() {
        setMaps(WriterEntity.load("User.txt",getMaps(),User.class));
    }

    public ArrayList<User> listUser(){
        System.out.println(getMaps().size());
        ArrayList<Thing> things = new ArrayList<>(getMaps().values());
        ArrayList<User> users = new ArrayList<>();
        for (Thing t: things){
            users.add((User)t);
        }
        return users;
    }



}
