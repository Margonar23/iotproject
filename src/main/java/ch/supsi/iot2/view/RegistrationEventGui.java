package ch.supsi.iot2.view;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.builder.UserBuilder;
import ch.supsi.iot2.controller.CommandSendRegistration;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.User;
import ch.supsi.iot2.controller.ZeroMqServer;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.time.LocalDate;


/**
 * Created by Margonar on 19.12.2016.
 */
public class RegistrationEventGui extends Application {

    Room room;

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("New Event Registration");

        GridPane pane = new GridPane();
        pane.setHgap(8);
        pane.setVgap(8);
        Label userLogged = new Label("Nome utente loggato");
        Separator separator = new Separator();
        // title
        Label title = new Label("Title: ");
        TextField titleInput = new TextField();
        titleInput.setId("title");
        titleInput.setPromptText("Insert the event title");
        titleInput.setPrefWidth(200);
        // subtitle
        Label subtitle = new Label("Subtitle: ");
        TextField subtitleInput = new TextField();
        subtitleInput.setId("subTitle");
        subtitleInput.setPromptText("Insert the event subtitle");
        subtitleInput.setPrefWidth(200);
        Label start = new Label("Date: ");
        DatePicker startDatePicker = new DatePicker();
        if (RegistrationEventGui.this.room == null) {
            startDatePicker.setDisable(true);
        }
        startDatePicker.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactory =
                new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                for (LocalDate date : RegistrationEventGui.this.room.availabilty) {
                                    if (item.isEqual(date)) {
                                        setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }
                                }
                            }
                        };
                    }
                };
        startDatePicker.setOnAction(event -> {
            LocalDate date = startDatePicker.getValue();
        });
        startDatePicker.setDayCellFactory(dayCellFactory);
        startDatePicker.setValue(startDatePicker.getValue().plusDays(1));
        startDatePicker.setPromptText("Pick the date");
        startDatePicker.setPrefWidth(200);

        Label users = new Label("Supervisor");

        ListView<User> usersList = new ListView<>();
        ObservableList<User> user = FXCollections.observableArrayList();
        UserBuilder b = new UserBuilder();
        User user1 = b.build();
        b.setNome("Giuliano Gremlich")
                .setEmail("g.grem@gmail.com")
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(null)
                .setRuolo("Docente");
        User user2 = b.build();
        b.setNome("Luca Margonar")
                .setEmail("luca.margonar@gmail.com")
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(null)
                .setRuolo("Studente");
        user.add(user1);
        user.add(user2);

        usersList.setItems(user);
        usersList.setPrefWidth(200);
        RoomBuilder builder = new RoomBuilder();
        Room room1 = builder.build();
        builder.setChair(22)
                .setFloor(3)
                .setNumber(311)
                .setHasBeamer(true)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(ZeroMqServer.getInstance())
                .setRequest("Registration")
                .setMyId();
        Room room2 = builder.build();
        builder.setChair(50)
                .setFloor(1)
                .setNumber(101)
                .setHasBeamer(true)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(ZeroMqServer.getInstance())
                .setRequest("Registration")
                .setMyId();

        Label rooms = new Label("Room");
        ListView<Room> roomsList = new ListView<>();
        ObservableList<Room> room = FXCollections.observableArrayList();
        room.add(room1);
        room.add(room2);
        roomsList.setPrefWidth(200);


        roomsList.setItems(room);

        roomsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Room>() {
            @Override
            public void changed(ObservableValue<? extends Room> observable, Room oldValue, Room newValue) {
                RegistrationEventGui.this.room = newValue;
                startDatePicker.setDisable(false);
                System.out.println(RegistrationEventGui.this.room.toString());
            }
        });

        Label price = new Label("Price: ");
        TextField priceInput = new TextField();
        priceInput.setId("price");
        priceInput.setPromptText("Insert the event price");
        priceInput.setPrefWidth(200);
        Label maxSeats = new Label("Max Seats: ");
        TextField maxSeatsInput = new TextField();
        maxSeatsInput.setId("maxSeats");
        maxSeatsInput.setPromptText("Insert the event max Seats");
        maxSeatsInput.setPrefWidth(200);

        Button registerEvent = new Button("Register event");
        registerEvent.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CommandSendRegistration.create(titleInput.getText(),subtitleInput.getText(),startDatePicker.getValue(), Double.parseDouble(priceInput.getText()),roomsList.getSelectionModel().getSelectedItem().getRoom(),usersList.getSelectionModel().getSelectedItem().getUser(),Integer.parseInt(maxSeatsInput.getText()),null).execute();

                primaryStage.close();
            }
        });

        pane.add(userLogged, 0, 0);
        pane.add(title, 0, 3);
        pane.add(titleInput, 1, 3);
        pane.add(subtitle, 0, 4);
        pane.add(subtitleInput, 1, 4);
        pane.add(start, 0, 7);
        pane.add(startDatePicker, 1, 7);
        pane.add(users, 0, 6);
        pane.add(usersList, 1, 6);
        pane.add(rooms, 0, 5);
        pane.add(roomsList, 1, 5);
        pane.add(price, 0, 8);
        pane.add(priceInput, 1, 8);
        pane.add(maxSeats, 0, 9);
        pane.add(maxSeatsInput, 1, 9);
        pane.add(registerEvent, 1, 11);

        Scene scene = new Scene(pane, 400, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

    }


}
