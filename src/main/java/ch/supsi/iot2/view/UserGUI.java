package ch.supsi.iot2.view;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

public class UserGUI extends Application {
    String softwareVersion = "1.0";
    TextArea consoleLog;
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Project Iot2");

        ToggleButton informationRequest = new ToggleButton();
        informationRequest.setId("informationRequest");
        File information = new File(getClass().getClassLoader().getResource("information.png").getFile());
        Label informationLabel = new Label("Information Request");
        informationLabel.setLabelFor(informationRequest);
        Image infoImg = new Image(information.toURI().toString());
        ImageView infoImage = new ImageView(infoImg);
        informationRequest.setGraphic(infoImage);

        ToggleButton registrationRequest = new ToggleButton();
        registrationRequest.setId("registrationRequest");
        File registrationFile = new File(getClass().getClassLoader().getResource("registration.png").getFile());
        Label registrationLabel = new Label("Registration Reguest");
        registrationLabel.setLabelFor(registrationRequest);
        Image registrationImg = new Image(registrationFile.toURI().toString());
        ImageView registrationImage = new ImageView(registrationImg);
        registrationRequest.setGraphic(registrationImage);

        BorderPane borderPane = new BorderPane(null, createMenu(primaryStage),informationRequest, null,registrationRequest );
        Scene scene = new Scene(borderPane, 300, 200, Color.WHITE);
        borderPane.getLeft().setTranslateY(50);
        borderPane.getLeft().setTranslateX(50);
        borderPane.getRight().setTranslateY(50);
        borderPane.getRight().setTranslateX(-50);
        primaryStage.setScene(scene);
        primaryStage.show();

        informationRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new InformationGui().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        registrationRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new RegistrationGui().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void openAboutWindow(String softwareVersion) {
        Stage aboutStage = new Stage();
        TextArea about = new TextArea("Project IoT IngSw 2 \n" +
                "Version: "+ softwareVersion + " \n" +
                "For educational use only. \n" +
                "\n\n"+
                "Powered by: M.G.M");
        about.setId("aboutText");
        about.setEditable(false);
        Scene aboutDescription = new Scene(new BorderPane(about, null, null, null, null), 400, 400, Color.AZURE);
        aboutStage.setScene(aboutDescription);
        aboutStage.show();
    }

    public MenuBar createMenu(Stage primaryStage) {
        MenuBar menuBar = new MenuBar();

        Menu menu = new Menu("File");
        MenuItem newItem = new MenuItem("New");
        MenuItem saveItem = new MenuItem("Save");
        MenuItem exitItem = new MenuItem("Exit");

        menu.getItems().add(newItem);
        menu.getItems().add(saveItem);
        menu.getItems().add(new SeparatorMenuItem());
        menu.getItems().add(exitItem);
        exitItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    System.exit(0);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        Menu info = new Menu("Info");
        info.setId("info");
        MenuItem aboutItem = new MenuItem("about");
        aboutItem.setId("about");
        info.getItems().add(new MenuItem("help"));
        info.getItems().add(aboutItem);
        aboutItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                openAboutWindow(softwareVersion);
            }
        });
        menuBar.getMenus().add(menu);
        menuBar.getMenus().add(info);

        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());

        return menuBar;
    }
}
