package ch.supsi.iot2.view;

import ch.supsi.iot2.controller.CommandGetAllRoom;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by vincenzo on 11/11/16.
 */
public class InformationGui extends Application {

    private TextArea consoleLog = new TextArea();

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Richiedi Informazioni");
        ToggleButton roomButton = new ToggleButton();
        roomButton.setId("roomButton");
        File roomIcon = new File(getClass().getClassLoader().getResource("roomIcon.png").getFile());
        Image roomImg = new Image(roomIcon.toURI().toString());
        ImageView roomImage = new ImageView(roomImg);
        roomButton.setGraphic(roomImage);

        GridPane pane = new GridPane();
        pane.add(roomButton, 0, 0);

        BorderPane informationPane = new BorderPane(null, null, consoleLog, null, pane);
        Scene informationScene = new Scene(informationPane, 700, 200);
        consoleLog.setMinSize(informationScene.getWidth() / 2, informationScene.getHeight() / 2);
        consoleLog.setEditable(false);
        primaryStage.setScene(informationScene);
        primaryStage.setResizable(false);
        primaryStage.show();
        roomButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CommandGetAllRoom.create(consoleLog).execute();
            }
        });
    }


}
