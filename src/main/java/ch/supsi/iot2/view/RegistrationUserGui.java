package ch.supsi.iot2.view;

import ch.supsi.iot2.controller.CommandSendRegistration;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by Grem on 03.11.16.
 */
public class RegistrationUserGui extends Application {
    private TextArea consoleLog = new TextArea();

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Effettua una registrazione");
        ToggleButton user = new ToggleButton();
        user.setId("user");
        File userFile = new File(getClass().getClassLoader().getResource("user.png").getFile());
        Label userLabel = new Label("    New User");
        userLabel.setLabelFor(user);
        Image userImg = new Image(userFile.toURI().toString());
        ImageView userImage = new ImageView(userImg);
        user.setGraphic(userImage);



        BorderPane registrationPane = new BorderPane(user, null,null, consoleLog,null);
        Scene registrationScene = new Scene(registrationPane, 500, 400);
        consoleLog.setEditable(false);
        primaryStage.setScene(registrationScene);
        primaryStage.setResizable(false);
        primaryStage.show();
        user.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                userDataInsert();
            }
        });
    }

    private void userDataInsert() {
        Stage insertUserData = new Stage();
        insertUserData.setWidth(500);

        Label name = new Label("inserisci nome ");
        name.setMinWidth(150.0);
        TextField nameInsert = new TextField();
        nameInsert.setId("nameInsert");
        nameInsert.setPromptText("inserisci il tuo nome completo:");
        nameInsert.setPrefWidth(300);
        Label email = new Label("inserisci E-Mail ");
        TextField emailInsert = new TextField();
        emailInsert.setId("emailInsert");
        emailInsert.setPromptText("inserisci il tuo indirizzo E-Mail:");
        emailInsert.setPrefWidth(300);
        Label ruoloLabel = new Label("ruolo: ");
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Student",
                        "Docente",
                        "boh"
                );
        final ComboBox ruolo = new ComboBox(options);
        ruolo.setId("ruoloCombo");

        GridPane gridPane = new GridPane();
        Button sendButton = new Button("SEND REGISTRATION");
        sendButton.setId("sendButton");
        gridPane.setVgap(2);
        gridPane.addRow(1, name, nameInsert);
        gridPane.addRow(2, email, emailInsert);
        gridPane.addRow(3, ruoloLabel, ruolo);
        gridPane.addRow(6, sendButton);

        Scene insertUserScene = new Scene(gridPane, 300, 250, Color.AZURE);
        insertUserData.setScene(insertUserScene);
        insertUserData.show();

        sendButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CommandSendRegistration.create(nameInsert.getText(), emailInsert.getText(), ruolo.getSelectionModel().getSelectedItem().toString(), consoleLog).execute();
                insertUserData.close();
            }
        });

    }
}
