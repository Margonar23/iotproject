package ch.supsi.iot2.view;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by Grem on 13.01.17.
 */
public class RegistrationGui extends Application {

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Effettua una Registratione");
        Label topLabel = new Label("Scegli un'entità da registrare");

        ToggleButton roomButton = new ToggleButton();
        roomButton.setId("roomButton");
        File room = new File(getClass().getClassLoader().getResource("roomIcon.png").getFile());
        Label roomLabel = new Label("New Room");
        roomLabel.setLabelFor(roomButton);
        Image roomImg = new Image(room.toURI().toString());
        ImageView roomImage = new ImageView(roomImg);
        roomButton.setGraphic(roomImage);

        ToggleButton userButton = new ToggleButton();
        userButton.setId("userButton");
        File user = new File(getClass().getClassLoader().getResource("user.png").getFile());
        Label userLabel = new Label("New User");
        userLabel.setLabelFor(userButton);
        Image userImg = new Image(user.toURI().toString());
        ImageView userImage = new ImageView(userImg);
        userButton.setGraphic(userImage);

        ToggleButton eventButton = new ToggleButton();
        eventButton.setId("eventButton");
        File button = new File(getClass().getClassLoader().getResource("event.png").getFile());
        Label buttonLabel = new Label("New Event");
        buttonLabel.setLabelFor(eventButton);
        Image buttonImg = new Image(button.toURI().toString());
        ImageView buttonImage = new ImageView(buttonImg);
        eventButton.setGraphic(buttonImage);

        BorderPane borderPane = new BorderPane(eventButton, topLabel,roomButton, null,userButton );
        Scene scene = new Scene(borderPane, 300, 200, Color.WHITE);
        borderPane.getTop().setTranslateY(25);
        borderPane.getTop().setTranslateX(60);
        borderPane.getLeft().setTranslateY(60);
        borderPane.getLeft().setTranslateX(10);
        borderPane.getRight().setTranslateY(60);
        borderPane.getRight().setTranslateX(-10);

        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();

        roomButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new RegistrationRoomGui().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        userButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new RegistrationUserGui().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        eventButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new RegistrationEventGui().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
