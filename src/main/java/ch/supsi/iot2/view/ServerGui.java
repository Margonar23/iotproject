package ch.supsi.iot2.view;

import ch.supsi.iot2.controller.CommandController;
import ch.supsi.iot2.controller.CommandNetworkStructureInit;
import ch.supsi.iot2.controller.ZeroMqServer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by Grem on 20.10.16.
 */
public class ServerGui extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ZeroMq Server IOT2");

        ToggleButton avviaServer = new ToggleButton();
        avviaServer.setId("start");
        File startFile = new File(getClass().getClassLoader().getResource("play.png").getFile());
        Image startImage = new Image(startFile.toURI().toString());
        ImageView startIcon = new ImageView(startImage);
        avviaServer.setGraphic(startIcon);

        ToggleButton spegniServer = new ToggleButton();
        spegniServer.setId("shutDown");
        File spegniFile = new File(getClass().getClassLoader().getResource("exit.png").getFile());
        Image spegniImage = new Image(spegniFile.toURI().toString());
        ImageView spegniIcon = new ImageView(spegniImage);
        spegniServer.setGraphic(spegniIcon);

        Button pulisciLog = new Button("Clean Log");
        pulisciLog.setId("cleanLog");
        TextArea console = new TextArea();

        Button saveMap = new Button("Save Entity");
        saveMap.setId("save");
        Button loadMap = new Button("Load Entity");
        loadMap.setId("load");
        BorderPane centerPane = new BorderPane(pulisciLog,saveMap,null,loadMap,null);
        BorderPane principalPanel = new BorderPane(centerPane, null, avviaServer, console, spegniServer);
        Scene principale = new Scene(principalPanel, 800, 500, Color.AQUA);
        centerPane.getTop().setTranslateX(290);
        centerPane.getTop().setTranslateY(principale.getHeight() / 6);
        centerPane.getBottom().setTranslateX(290);
        centerPane.getBottom().setTranslateY(-(principalPanel.getHeight() / 6));

        principalPanel.getLeft().setTranslateY(principale.getHeight() / 6);
        principalPanel.getLeft().setTranslateX(30);
        principalPanel.getRight().setTranslateY(principalPanel.getHeight() / 6);
        principalPanel.getRight().setTranslateX(-30);
        final CommandController[] initCommand = new CommandController[1];
        avviaServer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                initCommand[0] =  CommandNetworkStructureInit.create(console);
                initCommand[0].execute();
                avviaServer.setDisable(true);
            }
        });

        spegniServer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                initCommand[0].undo();
                avviaServer.setDisable(false);
            }
        });
        pulisciLog.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                console.clear();
            }
        });

        saveMap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                ZeroMqServer.getInstance().saveEntity();
            }
        });
        loadMap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ZeroMqServer.getInstance().loadEntity();
            }
        });
        primaryStage.setScene(principale);
        primaryStage.show();

    }


}
