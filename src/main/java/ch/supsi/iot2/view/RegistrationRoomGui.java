package ch.supsi.iot2.view;

import ch.supsi.iot2.controller.CommandSendRegistration;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by Grem on 03.11.16.
 */
public class RegistrationRoomGui extends Application {
    private TextArea consoleLog = new TextArea();

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Effettua una registrazione");
        ToggleButton room = new ToggleButton();
        room.setId("room");
        File roomFile = new File(getClass().getClassLoader().getResource("roomIcon.png").getFile());
        Label roomLabel = new Label("    Room");
        roomLabel.setLabelFor(room);

        Image roomImg = new Image(roomFile.toURI().toString());
        ImageView roomImage = new ImageView(roomImg);
        room.setGraphic(roomImage);

        BorderPane registrationPane = new BorderPane(room, null,null ,consoleLog,null );
        Scene registrationScene = new Scene(registrationPane, 500, 400);
        consoleLog.setEditable(false);
        primaryStage.setScene(registrationScene);
        primaryStage.setResizable(false);
        primaryStage.show();
        room.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                roomDataInsert();
            }
        });
    }

    private void roomDataInsert() {
        Stage insertRoomData = new Stage();

        insertRoomData.setWidth(400);
        Label floor = new Label("inserisci piano ");
        floor.setMinWidth(200.0);
        TextField floorInsert = new TextField();
        floorInsert.setId("floorInsert");
        floorInsert.setPromptText("inserisci il piano");

        Label roomNumber = new Label("inserisci numero aula ");
        TextField roomNumberInsert = new TextField();
        roomNumberInsert.setPromptText("inserisci il numero della room");
        roomNumberInsert.setId("roomNumberInsert");
        Label chairNumber = new Label("inserisci numero sedie ");
        TextField chairNumberInsert = new TextField();
        chairNumberInsert.setPromptText("inserisci il numero di sedie contenute");
        chairNumberInsert.setId("chairNumberInsert");
        Label beamer = new Label("l'aula ha il beamer?");
        CheckBox beamerInsert = new CheckBox();

        Label computer = new Label("l'aula ha il computer?");
        CheckBox computerInsert = new CheckBox();

        GridPane gridPane = new GridPane();
        Button sendButton = new Button("SEND REGISTRATION");
        sendButton.setId("sendButton");
        gridPane.setVgap(2);
        gridPane.addRow(1, floor, floorInsert);
        gridPane.addRow(2, roomNumber, roomNumberInsert);
        gridPane.addRow(3, chairNumber, chairNumberInsert);
        gridPane.addRow(4, beamer, beamerInsert);
        gridPane.addRow(5, computer, computerInsert);
        gridPane.addRow(6, sendButton);

        Scene insertRoomScene = new Scene(gridPane, 300, 250, Color.AZURE);
        insertRoomData.setScene(insertRoomScene);
        insertRoomData.show();

        sendButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CommandSendRegistration.create(chairNumberInsert.getText(),floorInsert.getText(),roomNumberInsert.getText(),beamerInsert.isSelected(),computerInsert.isSelected(),consoleLog).execute();
                insertRoomData.close();
            }
        });


    }
}
