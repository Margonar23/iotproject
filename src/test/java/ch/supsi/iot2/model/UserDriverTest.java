package ch.supsi.iot2.model;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.builder.UserBuilder;
import ch.supsi.iot2.controller.*;
import org.junit.Assert;
import org.junit.Test;

import javax.jws.soap.SOAPBinding;

import static org.junit.Assert.assertEquals;

/**
 * Created by Grem on 16.01.17.
 */
public class UserDriverTest {

    @Test
    public void UserDriverTest(){
        Driver driver = new UserDriver();
        Assert.assertEquals("User",driver.getIdModule());
    }

    @Test
    public void subScribeRequestTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(6010);
        server.serverInit(null);
        UserBuilder builder = new UserBuilder();
        User user = builder.build();
        builder.setNome("prova")
                .setEmail("Test")
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(null)
                .setRuolo("Test")
                .setServer(server);
        UserDriver driver = new UserDriver();
        driver.setServer(server);
        Module module = new RegModule();
        module.setServerZMQ(server);
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.pubToDriver("User",user.toJson());
            }
        }).start();
        assertEquals(user,driver.subscribeRequest());
    }
    @Test
    public void registrationTest(){
        UserBuilder builder = new UserBuilder();
        User user = builder.build();
        builder.setNome("prova")
                .setEmail("Test")
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(null)
                .setRuolo("Test")
                .setServer(null);
        UserDriver driver = new UserDriver();
        driver.registration(user);
        assertEquals(user,driver.getMaps().get(user.getEmail()));
    }
    @Test
    public void manageRequestTest(){
        UserBuilder builder = new UserBuilder();
        User user = builder.build();
        builder.setNome("prova")
                .setEmail("Test")
                .setRequest("Registration")
                .setMyID()
                .setConsoleLog(null)
                .setRuolo("Test")
                .setServer(null);
        UserDriver driver = new UserDriver();
        driver.setServer(ZeroMqServer.getInstance());
        driver.manageRequest(user);
        assertEquals(user,driver.getMaps().get(user.getEmail()));
    }
}
