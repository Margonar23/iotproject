package ch.supsi.iot2.model;

import ch.supsi.iot2.builder.EventBuilder;
import ch.supsi.iot2.controller.Event;
import ch.supsi.iot2.controller.RegModule;
import ch.supsi.iot2.controller.ZeroMqServer;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Grem on 16.01.17.
 */
public class EventDriverTest {

    @Test
    public void RoomDriverTest(){
        Driver driver = new EventDriver();
        Assert.assertEquals("Event",driver.getIdModule());
    }

    @Ignore
    @Test
    public void subScribeRequestTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(6030);
        server.serverInit(null);
        EventBuilder builder = new EventBuilder();
        Event event = builder.build();
        builder.setTitle("Test")
                .setSubtitle("Test")
                .setStart(null)
                .setSupervisor(null)
                .setRoom(null)
                .setPrice(1)
                .setMaxSeats(1)
                .setServer(server)
                .setConsoleLog(null)
                .setRequest("Registration");
        EventDriver driver = new EventDriver();
        driver.setServer(server);
        RegModule module = new RegModule();
        module.setServerZMQ(server);
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.pubToDriver("Event",event.toJson());
            }
        }).start();
        assertEquals(event,driver.subscribeRequest());
    }

    @Test
    public void registrationTest(){
        EventBuilder builder = new EventBuilder();
        Event event = builder.build();
        builder.setTitle("Test")
                .setSubtitle("Test")
                .setStart(null)
                .setSupervisor(null)
                .setRoom(null)
                .setPrice(1)
                .setMaxSeats(1)
                .setServer(null)
                .setConsoleLog(null)
                .setRequest("Registration");
        EventDriver driver = new EventDriver();
        driver.registration(event);
        assertEquals(event,driver.getMaps().get(event.getTitle()+""));
    }
    @Test
    public void manageRequestTest(){
        EventBuilder builder = new EventBuilder();
        Event event = builder.build();
        builder.setTitle("Test")
                .setSubtitle("Test")
                .setStart(null)
                .setSupervisor(null)
                .setRoom(null)
                .setPrice(1)
                .setMaxSeats(1)
                .setServer(null)
                .setConsoleLog(null)
                .setRequest("Registration");
        EventDriver driver = new EventDriver();
        driver.setServer(ZeroMqServer.getInstance());
        driver.manageRequest(event);
        assertEquals(event,driver.getMaps().get(event.getTitle()+""));
    }
}
