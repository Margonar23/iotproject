package ch.supsi.iot2.model;

/**
 * Created by Margonar on 28.11.2016.
 */

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.controller.RegModule;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.ZeroMqServer;
import org.junit.Assert;
import org.junit.Test;

import java.text.NumberFormat;

import static org.junit.Assert.assertEquals;

public class RoomDriverTest {

    @Test
    public void RoomDriverTest(){
        Driver driver = new RoomDriver();
        Assert.assertEquals("Room",driver.getIdModule());
    }

    @Test
    public void subScribeRequestTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(6000);
        server.serverInit(null);
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(1)
                .setFloor(1)
                .setNumber(1)
                .setHasBeamer(false)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(server)
                .setRequest("Registration")
                .setMyId();
        RoomDriver driver = new RoomDriver();
        driver.setServer(server);
        RegModule module = new RegModule();
        module.setServerZMQ(server);
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.pubToDriver("Room",room.toJson());
            }
        }).start();
        assertEquals(room,driver.subscribeRequest());
    }

    @Test
    public void registrationTest(){
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(1)
                .setFloor(1)
                .setNumber(1)
                .setHasBeamer(false)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(null)
                .setRequest("Registration")
                .setMyId();
        RoomDriver driver = new RoomDriver();
        driver.registration(room);
        assertEquals(room,driver.getMaps().get(room.getNumber()+""));
    }
    @Test
    public void manageRequestTest(){
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(1)
                .setFloor(1)
                .setNumber(1)
                .setHasBeamer(false)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(null)
                .setRequest("Registration")
                .setMyId();
        RoomDriver driver = new RoomDriver();
        driver.setServer(ZeroMqServer.getInstance());
        driver.manageRequest(room);
        assertEquals(room,driver.getMaps().get(room.getNumber()+""));
    }
}
