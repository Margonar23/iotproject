package ch.supsi.iot2.view;

import javafx.stage.Stage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testfx.api.FxRobot;

import static org.testfx.api.FxToolkit.registerPrimaryStage;
import static org.testfx.api.FxToolkit.setupApplication;

/**
 * Created by vincenzo on 14/01/17.
 */
public class InformationGuiTest extends FxRobot {

    private static Stage primaryStage;

    private int stepNo;
    String s;

    @BeforeClass
    public static void setupSpec() throws Exception {
        if (Boolean.getBoolean("headless")) {
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.order", "sw");
            System.setProperty("prism.text", "t2k");
            System.setProperty("java.awt.headless", "true");
        }
        primaryStage = registerPrimaryStage();
    }

    @Before
    public void setUp() throws Exception {


    }


    @Before
    public void setup() throws Exception {
        setupApplication(InformationGui.class);
    }

    @Test
    public void informationGuiTest() {
        testRoomButton();

    }

    private void testRoomButton() {
        clickOn("#roomButton");
    }
}
