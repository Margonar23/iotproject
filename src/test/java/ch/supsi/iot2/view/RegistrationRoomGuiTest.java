package ch.supsi.iot2.view;

import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.*;
import org.testfx.api.FxRobot;

import static org.testfx.api.FxToolkit.registerPrimaryStage;
import static org.testfx.api.FxToolkit.setupApplication;

/**
 * Created by vincenzo on 05/12/16.
 */
public class RegistrationRoomGuiTest extends FxRobot{
    private static Stage primaryStage;
    @BeforeClass
    public static void setupSpec() throws Exception {
        if (Boolean.getBoolean("headless")) {
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.order", "sw");
            System.setProperty("prism.text", "t2k");
            System.setProperty("java.awt.headless", "true");
        }
        primaryStage = registerPrimaryStage();
    }

    @Before
    public void setUp() throws Exception {


    }


    @Before
    public void setup() throws Exception {
        setupApplication(RegistrationRoomGui.class);
    }
    @Test
    public void informationGuiTest() {
        roomDataInsert();

    }


    private void roomDataInsert() {
      //  sleep(55555);
        clickOn("#room");


        clickOn("#floorInsert").write("3");
        clickOn("#roomNumberInsert").write("23");
        clickOn("#chairNumberInsert").write("152");

        clickOn("#sendButton");
    }
}
