package ch.supsi.iot2.view;

import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.*;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit.ApplicationTest;

import static org.testfx.api.FxToolkit.registerPrimaryStage;
import static org.testfx.api.FxToolkit.setupApplication;

/**
 * Created by vincenzo on 05/12/16.
 */
public class RegistrationUserGuiTest extends FxRobot {



    private static Stage primaryStage;

    private int stepNo;
    String s;

    @BeforeClass
    public static void setupSpec() throws Exception {
        if (Boolean.getBoolean("headless")) {
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.order", "sw");
            System.setProperty("prism.text", "t2k");
            System.setProperty("java.awt.headless", "true");
        }
        primaryStage = registerPrimaryStage();
    }


    @Before
    public void setup() throws Exception {
        setupApplication(RegistrationUserGui.class);
    }
    @Test
    public void roomDataInsert() {
        clickOn("#user");
        clickOn("#nameInsert").write("Vincenzo");
        clickOn("#emailInsert").write("mail@gmail.com");
        clickOn("#ruoloCombo");
        press(KeyCode.DOWN);
        release(KeyCode.DOWN);

        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        clickOn("#sendButton");

        //  TextField name = (TextField) find("#nameInsert");
      //  name.setText("maria");
      //  Assert.assertEquals("maria", name.getText());
      //  sleep(5575);
    }

    public <T extends Node> T find(final String query) {
        return (T) lookup(query).queryAll().iterator().next();
    }

}
