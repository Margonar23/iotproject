package ch.supsi.iot2.view;

import javafx.stage.Stage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testfx.api.FxRobot;

import static org.testfx.api.FxToolkit.registerPrimaryStage;
import static org.testfx.api.FxToolkit.setupApplication;

/**
 * Created by vincenzo on 15/01/17.
 */
public class RegistrationEventGuiTest extends FxRobot {
    private static Stage primaryStage;

    @BeforeClass
    public static void setupSpec() throws Exception {
        if (Boolean.getBoolean("headless")) {
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.order", "sw");
            System.setProperty("prism.text", "t2k");
            System.setProperty("java.awt.headless", "true");
        }
        primaryStage = registerPrimaryStage();
    }


    @Before
    public void setup() throws Exception {
        setupApplication(RegistrationEventGui.class);
    }

    @Test
    public void registrationEventGuiTest() {
        registrationEvent();

    }
  private void registrationEvent(){
      clickOn("#title").write("IOT");
      clickOn("#subTitle").write("conferenza");
      clickOn("#price").write("10");
      clickOn("#maxSeats").write("43");
  }

}
