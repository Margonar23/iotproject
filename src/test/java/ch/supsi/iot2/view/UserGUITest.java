package ch.supsi.iot2.view;

import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.*;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.testfx.api.FxToolkit.registerPrimaryStage;
import static org.testfx.api.FxToolkit.setupApplication;

/**
 * Created by vincenzo on 10/12/16.
 */
public class UserGUITest extends FxRobot {
    private static Stage primaryStage;
    @BeforeClass
    public static void setupSpec() throws Exception {
        if (Boolean.getBoolean("headless")) {
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.order", "sw");
            System.setProperty("prism.text", "t2k");
            System.setProperty("java.awt.headless", "true");
        }
        primaryStage = registerPrimaryStage();
    }


    @Before
    public void setup() throws Exception {
        setupApplication(UserGUI.class);
    }
    @Test
    public void informationGuiTest() {
        //sendInforequestTest();
        //sendRegistrationrequestTest();
        aboutTest();

    }
    private void sendInforequestTest(){
        clickOn("#informationRequest");

    }
    private void aboutTest(){
        clickOn("#info");
        clickOn("#about");

    }
    private void sendRegistrationrequestTest(){
        clickOn("#registrationRequest");

    }


    }

