package ch.supsi.iot2.builder;

/**
 * Created by Margonar on 06.12.2016.
 */

import ch.supsi.iot2.controller.ZeroMqServer;
import ch.supsi.iot2.controller.Room;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RoomBuilderTest {

    Room room;
    RoomBuilder builder;

    @Before
    public void setup(){
        builder = new RoomBuilder();
        room = builder.build();
    }

    @Test
    public void setChairTest(){
        builder.setChair(1);
        assertEquals(1,room.getChair());
    }

    @Test
    public void setNumberTest(){
        builder.setNumber(3);
        assertEquals(3,room.getNumber());
    }

    @Test
    public void setHasBeamerTest(){
        builder.setHasBeamer(true);
        assertEquals(true,room.isHasBeamer());
    }

    @Test
    public void setHasComputerTest(){
        builder.setHasComputer(true);
        assertEquals(true,room.isHasComputer());
    }

    @Test
    public void setRequestTest(){
        builder.setRequest("request");
        assertEquals("request",room.getRequest());
    }

    @Test
    public void setMyIDTest(){
        builder.setMyId();
        assertEquals("Room",room.getMyID());
    }
    @Test
    public void setServerTest(){
        ZeroMqServer server = ZeroMqServer.getInstance();
        builder.setServer(server);
        assertEquals(server,room.getServer());
    }

@Test
public void setConsoleLogTest(){
    builder.setConsoleLog(null);
    assertEquals(null,room.getConsoleLog());
}






}
