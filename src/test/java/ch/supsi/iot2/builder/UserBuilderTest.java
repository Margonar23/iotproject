package ch.supsi.iot2.builder;

/**
 * Created by Margonar on 12.12.2016.
 */

import ch.supsi.iot2.controller.ZeroMqServer;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserBuilderTest {
    User user;
    UserBuilder builder;

    @Before
    public void setup() {
        builder = new UserBuilder();
        user = builder.build();
    }

    @Test
    public void setNomeTest() {
        builder.setNome("Luca");
        assertEquals("Luca", user.getNome());
    }

    @Test
    public void setRuoloTest() {
        builder.setRuolo("Docente");
        assertEquals("Docente", user.getRuolo());
    }

    @Test
    public void setEmailTest() {
        builder.setEmail("luca.margonar@student.supsi.ch");
        assertEquals("luca.margonar@student.supsi.ch", user.getEmail());
    }

    @Test
    public void setRequestTest() {
        builder.setRequest("Registration");
        assertEquals("Registration", user.getRequest());
    }

    @Test
    public void setMyIdTest() {
        builder.setMyID();
        assertEquals("User", user.getMyID());
    }

    @Test
    public void setThingForInfoTest() {
        RoomBuilder roomBuilder = new RoomBuilder();
        Room room = roomBuilder.build();
        builder.setThingForInfo(room);
        assertEquals(room, user.getThingForInfo());
    }

    @Test
    public void setServerTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        builder.setServer(server);
        assertEquals(server, user.getServer());
    }

    @Test
    public void setConsoleLogTest(){
        builder.setConsoleLog(null);
        assertEquals(null,user.getConsoleLog());
    }

}
