package ch.supsi.iot2.builder;

/**
 * Created by Margonar on 19.12.2016.
 */

import ch.supsi.iot2.controller.ZeroMqServer;
import ch.supsi.iot2.controller.Event;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.User;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class EventBuilderTest {
    Event event;
    User user;
    Room room;
    EventBuilder eventBuilder = new EventBuilder();
    UserBuilder userBuilder = new UserBuilder();
    RoomBuilder roomBuilder = new RoomBuilder();

    @Before
    public void setup() {
        event = eventBuilder.build();
        user = userBuilder.build();
        room = roomBuilder.build();
    }

    @Test
    public void setStartTest() {
        eventBuilder.setStart(LocalDate.of(2017, 01, 14));
        assertEquals(LocalDate.of(2017, 01, 14), event.getStart());
    }

    @Test
    public void setTitleTest() {
        eventBuilder.setTitle("Titolo");
        assertEquals("Titolo", event.getTitle());
    }

    @Test
    public void setSubtitleTest() {
        eventBuilder.setSubtitle("Sottotitolo");
        assertEquals("Sottotitolo", event.getSubtitle());
    }

    @Test
    public void setSupervisorTest() {
        userBuilder.setNome("luca")
                .setEmail("luca.margonar@gmail.com")
                .setRuolo("Docente");
        eventBuilder.setSupervisor(user);
        assertEquals(user, user.getUser());
    }

    @Test
    public void setRoomTest() {
        roomBuilder.setFloor(1)
                .setChair(10)
                .setNumber(200)
                .setHasBeamer(true)
                .setHasComputer(true);
        eventBuilder.setRoom(room);
        assertEquals(room, room.getRoom());
    }

    @Test
    public void setPriceTest(){
        eventBuilder.setPrice(10);
        assertEquals(10, event.getPrice(),0.0001);
    }

    @Test
    public void setMaxSeatsTest(){
        eventBuilder.setMaxSeats(20);
        assertEquals(20,event.getMaxSeats());
    }

    @Test
    public void setRequestTest(){
        eventBuilder.setRequest("Richiesta");
        assertEquals("Richiesta",event.getRequest());
    }
    @Test
    public void setConsoleLogTest(){
        eventBuilder.setConsoleLog(null);
        assertEquals(null,event.getConsoleLog());
    }

    @Test
    public void setServerTest(){
        eventBuilder.setServer(ZeroMqServer.getInstance());
        assertEquals(ZeroMqServer.getInstance(),event.getServer());
    }

}
