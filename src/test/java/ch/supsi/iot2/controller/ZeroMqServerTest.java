package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.RoomBuilder;
import org.junit.*;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Grem on 17.10.16.
 */
public class ZeroMqServerTest {

    @Test
    public void setConnectionPortTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(6040);
        Assert.assertEquals(6040, server.getPortForThing());
        Assert.assertEquals(6041, server.getPortForModulePub());
        Assert.assertEquals(6042, server.getPortForModuleReq());
        Assert.assertEquals(6043, server.getPortForPubDriver());
        Assert.assertEquals(6044, server.getPortForReqDriver());
    }

    @Test
    public void listenEntityRequestsTest() {
        ZeroMqServer server = ZeroMqServer.getInstance();
        server.setConnectionPort(6050);
        server.serverInit(null);
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(1)
                .setFloor(1)
                .setNumber(1)
                .setHasBeamer(false)
                .setHasComputer(false)
                .setConsoleLog(null)
                .setServer(server)
                .setRequest("Registration")
                .setMyId();
        new Thread(new Runnable() {
            @Override
            public void run() {
                room.sendRegistrationRequest();
            }
        }).start();
        server.listenEntityRequests();
        Assert.assertEquals("Registration/" + room.toJson(), server.getDataAtThings());

        Assert.assertEquals("Registration", server.manageEntityRequests());
        Assert.assertEquals(room.toJson(), server.getDataAtThings());
    }

}
