package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.controller.Room;
import javafx.scene.control.TextArea;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Created by vincenzo on 03/12/16.
 */
public class RoomTest {
    Room room;
    Thread thingThread;
    @Before
    public void setUpClass() throws Exception {
        RoomBuilder builder = new RoomBuilder();
        room = builder.build();
        builder.setFloor(2).setChair(2).setNumber(2).
                setHasComputer(false).setHasBeamer(false).setRequest("request");
        thingThread =  new Thread(new Runnable() {
            @Override
            public void run() {
                room.sendRegistrationRequest();
                System.out.println(room.getKey());
            }
        });
    }
    @Test
    public void getRequest() throws Exception {
        room.setRequest("request");
        Assert.assertEquals("request",room.getRequest());
    }

    @Test
    public void setRequest() throws Exception {
        room.setRequest("request");
        Assert.assertEquals("request",room.getRequest());
    }

    @Ignore
    @Test
    public void getConsoleLogRegistration() throws Exception {
        TextArea t = new TextArea();
        room.setConsoleLog(t);
        Assert.assertEquals(t,room.getConsoleLog());
    }

    @Ignore
    @Test
    public void setConsoleLogRegistration() throws Exception {
        TextArea t = new TextArea();
        room.setConsoleLog(t);
        Assert.assertEquals(t,room.getConsoleLog());
    }
    @Test
    public void toJson() throws Exception {
        String s = room.toJson();
        Room r = Room.fromJson(s);
        Assert.assertEquals(room,r);
    }

    @Test
    public void fromJson() throws Exception {
        String s = room.toJson();
        Room r = Room.fromJson(s);
        Assert.assertEquals(room,r);
    }

    @Test
    public void getChair() throws Exception {
        Assert.assertEquals(2,room.getChair());
    }

    @Test
    public void setChair() throws Exception {
        room.setChair(3);
        Assert.assertEquals(3,room.getChair());
    }

    @Test
    public void getFloor() throws Exception {
        Assert.assertEquals(2,room.getFloor());
    }

    @Test
    public void setFloor() throws Exception {
        room.setFloor(4);
        Assert.assertEquals(4,room.getFloor());
    }

    @Test
    public void getNumber() throws Exception {
        Assert.assertEquals(2,room.getNumber());
    }

    @Test
    public void setNumber() throws Exception {
        room.setNumber(5);
        Assert.assertEquals(5,room.getNumber());
    }

    @Test
    public void isHasBeamer() throws Exception {
        Assert.assertEquals(false,room.isHasBeamer());
    }

    @Test
    public void setHasBeamer() throws Exception {
        room.setHasBeamer(true);
        Assert.assertEquals(true,room.isHasBeamer());
    }

    @Test
    public void isHasComputer() throws Exception {
        Assert.assertEquals(false,room.isHasComputer());
    }

    @Test
    public void setHasComputer() throws Exception {
        room.setHasBeamer(true);
        Assert.assertEquals(true,room.isHasBeamer());
    }

    @Test
    public void sendRegistrationRequest() throws Exception {

    }



    @Test
    public void equals() throws Exception {
        RoomBuilder builder = new RoomBuilder();
        Room r = builder.build();
        builder.setFloor(2).setChair(2).setNumber(2).
                setHasComputer(false).setHasBeamer(false).setRequest("request").setMyId();
        RoomBuilder builder2 = new RoomBuilder();
        Room r1 = builder2.build();
        builder2.setFloor(2).setChair(2).setNumber(2).
                setHasComputer(false).setHasBeamer(false).setRequest("request").setMyId();

        Assert.assertEquals(true,r.equals(r1));
    }





}