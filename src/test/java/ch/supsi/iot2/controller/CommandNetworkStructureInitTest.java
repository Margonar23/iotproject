package ch.supsi.iot2.controller;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandNetworkStructureInitTest {

    CommandNetworkStructureInit command = null;
    @Ignore
    @Test
    public void executeTest() throws Exception {
        command = CommandNetworkStructureInit.create(null);
        command.execute();
        if (command.getInfoModule().isInterrupted() || command.getRegModule().isInterrupted() || command.getRoomDriver().isInterrupted() || command.getUserDriver().isInterrupted() || command.getEventDriver().isInterrupted()) {
            assertTrue(false);
        } else {
            assertTrue(true);
        }
        command.undo();

    }
    @Ignore
    @Test
    public void undoTest() throws Exception {
        command = CommandNetworkStructureInit.create(null);
        command.execute();
        command.undo();
        assertTrue(true);
    }

    @Test
    public void create() throws Exception {
        command = CommandNetworkStructureInit.create(null);
        if(command == null)
            assertTrue(false);
        else
            assertTrue(true);
    }

}