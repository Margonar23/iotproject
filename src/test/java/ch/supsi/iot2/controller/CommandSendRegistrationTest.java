package ch.supsi.iot2.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandSendRegistrationTest {
    CommandSendRegistration command;

    @Test
    public void execute() throws Exception {
        command = CommandSendRegistration.create("0","11","ruolo",null);
        command.execute();
        if(command.getSender().isInterrupted())
            assertTrue(false);
        else
            assertTrue(true);
        command.undo();
    }

    @Test
    public void undo() throws Exception {
        command = CommandSendRegistration.create("0","11","22",true,true,null);
        command.execute();
        command.undo();
        assertTrue(true);
    }

    @Test
    public void create() throws Exception {
        command = CommandSendRegistration.create("0","11","22",true,true,null);
        if(command == null)
            assertTrue(false);
        else
            assertTrue(true);
    }

    @Test
    public void create1() throws Exception {
        command = CommandSendRegistration.create("0","11","ruolo",null);
        if(command == null)
            assertTrue(false);
        else
            assertTrue(true);
    }

}