package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.controller.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Created by vincenzo on 05/12/16.
 */
public class UserTest {
    User user;
    CommandGetAllRoom commandGetAllRoom;
    CommandNetworkStructureInit commandInitNetwork;
    Room expected;
    Thread server;
    @Before
    public void setUp() throws Exception {

    }

    @Ignore
    @Test
    public void sendRequestFroGettingInformationsTest() throws Exception {
        RoomBuilder roomBuilder = new RoomBuilder();
        expected = roomBuilder.build();
        roomBuilder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
       ZeroMqServer.getInstance().serverInit(null);
        //server = new Thread(ZeroMqServer.getInstance());
        server.start();
        commandGetAllRoom = CommandGetAllRoom.create(null);
        commandGetAllRoom.execute();
        String t = ZeroMqServer.getInstance().getDataAtThings();
        System.out.println(t);
        Room room = Room.fromJson(t);
        Assert.assertEquals(expected,room);
        commandGetAllRoom.undo();
        server.interrupt();
    }


    @Test
    public void sendRequestTest(){


    }

}