package ch.supsi.iot2.controller;

/**
 * Created by Margonar on 18.10.2016.
 */


import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.model.RoomDriver;
import org.junit.*;

public class RegModuleTest {


    @Test
    public void RegModuleTest(){
        Module module = new RegModule();
        Assert.assertEquals("Registration",module.getIdModule());
    }

    @Test
    public void subscribeRequestTest() {
        Module module = new RegModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5080);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.subscribeRequest();
            }
        }).start();
        serverSimulator.pubRequest("Registration");
        Assert.assertEquals("Room", module.getTypeEntity());
        Assert.assertEquals(room.toJson(), module.getDataEntity());

    }
    @Ignore
    @Test
    public void sendDataToServerTest(){
        Module module = new RegModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5050);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.sendDataToServer("Test Passed");
            }
        }).start();
        serverSimulator.listenDataForClient();
        Assert.assertEquals("Test Passed", serverSimulator.getKey());
        Assert.assertEquals(200, module.getCodeKeySended());
    }

    @Test
    public void pubToDriverTest() {
        Module module = new RegModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5060);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        RoomDriver driver = new RoomDriver();
        driver.setServer(serverSimulator);
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.pubToDriver("Room",room.toJson());
            }
        }).start();
        driver.subscribeRequest();
        Assert.assertEquals(room.toJson(), driver.getReceived());
    }

    @Test
    public void runTest(){
        Thread t = new Thread(new InformationModule());
        t.start();
        t.interrupt();
        Assert.assertTrue(true);
    }

}
