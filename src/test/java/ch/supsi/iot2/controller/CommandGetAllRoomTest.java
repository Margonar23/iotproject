package ch.supsi.iot2.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Grem on 11.01.17.
 */
public class CommandGetAllRoomTest {


    CommandGetAllRoom command = null;

    @Test
    public void execute() throws Exception {
        command = CommandGetAllRoom.create(null);
        command.execute();
        if(command.getSender().isInterrupted())
            assertTrue(false);
        else
            assertTrue(true);
        command.undo();

    }

    @Test
    public void undo() throws Exception {
        command = CommandGetAllRoom.create(null);
        command.execute();
        command.undo();
        assertTrue(true);
    }

    @Test
    public void create() throws Exception {
        command = CommandGetAllRoom.create(null);
        if(command == null)
            assertTrue(false);
        else
            assertTrue(true);
    }

}