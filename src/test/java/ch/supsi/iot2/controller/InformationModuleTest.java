package ch.supsi.iot2.controller;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.model.RoomDriver;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Grem on 01.12.16.
 */
public class InformationModuleTest {

    @Test
    public void InformationModuleTest(){
        Module module = new InformationModule();
        Assert.assertEquals("Information",module.getIdModule());
    }

    @Test
    public void subscribeRequestTest() {
        Module module = new InformationModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5040);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.subscribeRequest();
            }
        }).start();
        serverSimulator.pubRequest("Information");
        Assert.assertEquals("Room", module.getTypeEntity());
        Assert.assertEquals(room.toJson(), module.getDataEntity());

    }

    @Test
    public void sendDataToServerTest(){
        Module module = new InformationModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5030);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.sendDataToServer("Test Passed");
            }
        }).start();
        serverSimulator.listenDataForClient();
        Assert.assertEquals("Test Passed", serverSimulator.getKey());
    }

    @Test
    public void pubToDriverTest() {
        Module module = new InformationModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5020);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        RoomBuilder builder = new RoomBuilder();
        Room room = builder.build();
        builder.setChair(0)
                .setFloor(0)
                .setNumber(0)
                .setHasBeamer(false)
                .setHasComputer(true)
                .setConsoleLog(null)
                .setRequest("Information")
                .setMyId()
                .setServer(ZeroMqServer.getInstance());
        serverSimulator.setDataAtThings(room.toJson());
        module.moduleInit();
        RoomDriver driver = new RoomDriver();
        driver.setServer(serverSimulator);
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.pubToDriver("Room",room.toJson());
            }
        }).start();
        driver.subscribeRequest();
        Assert.assertEquals(room.toJson(), driver.getReceived());
    }
    @Test
    public void listenInformationFromDriverTest(){
        InformationModule module = new InformationModule();
        ZeroMqServer serverSimulator =  ZeroMqServer.getInstance();
        serverSimulator.setConnectionPort(5070);
        serverSimulator.serverInit(null);
        module.setServerZMQ(ZeroMqServer.getInstance());
        module.moduleInit();
        RoomDriver driver = new RoomDriver();
        driver.setServer(serverSimulator);
        driver.driverInit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                module.listenInformationFromDriver();
            }
        }).start();
        driver.sendInformations("Test passed");
        Assert.assertEquals("Test passed", module.getDataEntity());
        Assert.assertEquals(200,driver.getCodeStatusSend());
    }
    @Test
    public void runTest(){
        Thread t = new Thread(new InformationModule());
        t.start();
        t.interrupt();
        Assert.assertTrue(true);
    }
}
