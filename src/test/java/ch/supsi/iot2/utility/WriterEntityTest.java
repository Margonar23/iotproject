package ch.supsi.iot2.utility;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.builder.UserBuilder;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.Thing;
import ch.supsi.iot2.controller.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;



/**
 * Created by vincenzo on 28/10/16.
 */
public class WriterEntityTest {
    Room t;
    User t1;
    Map<String, Thing> map = new HashMap<>();

    @Before
    public void setUpClass() throws Exception {
        new WriterEntity();
        RoomBuilder builderRoom = new RoomBuilder();
        t = builderRoom.build();
        builderRoom.setFloor(2)
                .setNumber(2)
                .setChair(2)
                .setHasComputer(false)
                .setHasBeamer(false)
                .setRequest("Registration");
        UserBuilder builderUser = new UserBuilder();
        t1 = builderUser.build();
        builderUser.setRequest("Registration")
                .setEmail("mail@mail.com")
                .setNome("nome")
                .setRuolo("studente");
        t.setKey("key");
        t1.setKey("key");
       map.put(t.getNumber()+"",t);
       map.put(t1.getEmail(),t1);

    }
    @Test
    public void writerPropertyUserTest(){
        String filename = "test.txt";
        WriterEntity.write(filename,map);
        Map<String,Thing> mapLoad = new HashMap<>();
        mapLoad = WriterEntity.load(filename, mapLoad, mapLoad.getClass());
        System.out.println(mapLoad.size());
        Assert.assertEquals(map.get(t.getNumber()),mapLoad.get(t.getNumber()));

    }
}
