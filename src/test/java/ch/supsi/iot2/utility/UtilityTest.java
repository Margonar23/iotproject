package ch.supsi.iot2.utility;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vincenzo on 14/01/17.
 */
public class UtilityTest {
    @Test
    public void  keyGeneratorTest(){
        new Utility();
       String key = Utility.keyGenerator();
       String anotherKey = Utility.keyGenerator();
        Assert.assertFalse(key.equals(anotherKey));

    }
}
