package ch.supsi.iot2.utility;

import ch.supsi.iot2.builder.RoomBuilder;
import ch.supsi.iot2.controller.Room;
import ch.supsi.iot2.controller.Thing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vincenzo on 14/01/17.
 */
public class JSonHelperTest {
    Gson gson;
    Room t;
    Map<String,Thing> map;
    @Before
    public void setUpClass() {
        new JsonHelper();
        gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategyGson()).create();
        map = new HashMap<>();
        RoomBuilder builderRoom = new RoomBuilder();
        t = builderRoom.build();
        builderRoom.setFloor(2)
                .setNumber(2)
                .setChair(2)
                .setHasComputer(false)
                .setHasBeamer(false)
                .setRequest("Registration");

        t.setKey("key");

        map.put(t.getNumber() + "", t);
    }
    @Test
    public  void toJsonTest(){

        String s = JsonHelper.toJson(map);
        Map<String,Room> map1 = new HashMap<>();
        JsonHelper.fromJson(s,map1);
        Assert.assertEquals(map.get(t.getNumber()),map1.get(t.getNumber()));
    }


}
